import React, { useState } from 'react';
import styled from 'styled-components';
import { EditorState, convertToRaw,ContentState } from 'draft-js';
import dynamic from 'next/dynamic'; 
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const Editor: any = dynamic(
  () => (import('react-draft-wysiwyg')as any).then((mod: any) => mod.Editor),
  { ssr: false }
)

const Container = styled.div`
  height: 60vh;
  display:flex;
  margin: 15px;
  flex-direction: column;
  align-items: center;
  color: black;
  background-color: white;
  border: 1px solid black;
  overflow: hidden;
`;
function EditChapter (props: any){
  const { onChange,value } = props;
  const [editorState,setEditorState] = useState(value);

  const handleEditor = (e: any) => {
    console.log(draftToHtml(convertToRaw(e.getCurrentContent())));
    onChange(draftToHtml(convertToRaw(e.getCurrentContent())));
    setEditorState(e);
    
  };
  return(
    <Container>
      <Editor
        editorState={editorState as EditorState}
        wrapperClassName="test"
        editorClassName="test"
        toolbar={{
          options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'emoji','history'],
        }}
        onEditorStateChange={(e: any) => handleEditor(e)}
          />
    </Container>
  )
};

export default EditChapter;