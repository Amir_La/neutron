import React, {  useEffect, useState } from 'react';
import styled from 'styled-components';
import Link from 'next/link'
import { parseCookies } from '../../helpers/cookies';
import { useCookies } from 'react-cookie';
import { useRouter } from 'next/router';

const Container = styled.div`
  width: 100%;
  height: 50px;
  display:flex;
  justify-content: space-between;
  align-items: center;
  padding: 5px 0px;
  color: black;
  background-color: white;
`; 
const Title = styled.div`
  font-weight: 900;
  padding: 5px;
`;
const Nav = styled(Link)`
  
`;
const DecoButton = styled.div`
  margin: 0px 5px;
  text-decoration: none;
  padding: 5px;
  color: black;
  border-bottom: 1px solid black;
  cursor: pointer;
`;
const NavContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  & a {
    margin: 0px 5px;
  text-decoration: none;
  padding: 5px;
  color: black;
  border-bottom: 1px solid black;
  }
`;
const ToggleThemeButton = styled.div`
  border-radius: 5px;
  padding: 5px;
  margin-right: 5px;
  cursor: pointer;
  color: white;
  background-color: black;
`;

function Navbar(props: any) {
  const [noUser,setNoUser] = useState(true);
  const [cookies, setCookie, removeCookie] = useCookies(["user"]);
  const router = useRouter();
  useEffect(() => {
    if(!cookies.user && !noUser) 
    {
      console.log("true")
      setNoUser(true);
    }
    else if(cookies.user && noUser)
    {
      console.log("false");
      setNoUser(false);
    }
  },[cookies])
  return (
    <Container>
      <Title> Neutron </Title>
      <NavContainer>
        <Nav href="/"> Accueil </Nav>
        { 
          !noUser ? <DecoButton onClick={() =>  { 
            setNoUser(false); 
            try {
              removeCookie('user');
            } catch (error) {
              console.log(error);
            }
            router.push("/connexion");
          }}> Deconnexion </DecoButton> : <Nav href="/connexion"> Connexion </Nav>
        }
        { 
          !noUser  && <Nav href="/profile"> Profil </Nav>
        }
      </NavContainer>
    </Container>
  );
}
Navbar.getInitialProps =  async ({ req, res }: any)=> {
  const data = parseCookies(req);
  return {
    data: data && data,
  }
}
export default Navbar;
