//import { type } from "node:os";
import Link from "next/link";
import React,{ FC, FunctionComponent } from "react";
import styled from 'styled-components';

const Container = styled.div`
  padding: 15px 5px; 
  width: 100%;
  margin: 5px;
  border-bottom: 2px solid #dee2e6;
  cursor: pointer;
`;
const ChapterLink = styled(Link)`
  cursor: pointer;
`;

const ItemLabel = styled.label`
  font-weight: 900;
  padding: 25px 15px;
  cursor: pointer;
  font-size: 20px;
  text-align: center;

`;

type IItemProps = {
  title: string;
  index: number;
  idBook: number;
}
const ChapterItem : FC<IItemProps> = (props: IItemProps) => (
  <ChapterLink href={`/books/${props.idBook}/chapters/${props.index}`} >
    <Container>
      <ItemLabel> {props.index} - {props.title} </ItemLabel>
    </Container>
  </ChapterLink>
)

export default ChapterItem;