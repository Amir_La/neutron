import React from 'react'
import Navbar from './navbar'
import styled from 'styled-components';

const Layout = ({children}: any) => {
  return (
    <>
    {/* Header */}
      <Navbar />
    {/* Body */}
      {children}
     {/* Footer */} 
    </>
  )
}

export default Layout;