//import { type } from "node:os";
import Link from "next/link";
import React,{ FC, FunctionComponent } from "react";
import styled from 'styled-components';

const Container = styled.div`
  padding: 5px;
  display: flex;
  flex-direction: column;
  margin: 5px;
  width: 200px;
  min-height: 200px;
  border: 2px solid #dee2e6;
  border-radius: 10px;
  padding: 15px;
  cursor: pointer;
  justify-content: space-between;
`;
const BookLink = styled.div`
  cursor: pointer;
`;

const ItemLabel = styled.label`
  font-weight: 900;
  padding-bottom: 5px;
  font-size: 20px;
  text-align: center;
`;

const ItemDescription = styled.div`
  word-break: break-word;
`;

const ItemPriority = styled.div`

`;

const DeleteContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const DeleteButton = styled.div`
  border: 1px solid red;
  background-color: red;
  border-radius: 5px;
  color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5px;
`;
const InfoContainer = styled.div`
  display: flex;
  justify-content: space-around;

  flex-direction: column;
  padding: 5px;
  height: 100%;
  align-items: center;
`;

const ItemStatus = styled.select`

`;
const StatusOptions = styled.option`

`;
const GoToPage = styled.a`
  padding: 10px 15px;
  font-weight: 900;
  background-color: #0077b6;
  color: #fff;
  border-radius: 5px;
  text-align: center;
`;

type IItemProps = {
  name: string;
  author: string;
  path: string;
  chapters: number;
  types: any[];
  description: string;
  id: number;
  content: string;
}
const Item : FC<IItemProps> = (props: IItemProps) => (
  <BookLink>
    <Container>
      <ItemLabel> {props.name} </ItemLabel>
    <InfoContainer>
      <ItemDescription> {props.description} </ItemDescription>
      <ItemPriority> de {props.author} </ItemPriority>
    </InfoContainer>
    <GoToPage href={`/${props.path}/${props.id}`}>{props.content}</GoToPage>
    </Container>
  </BookLink>
)

export default Item;