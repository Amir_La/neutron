import { useRouter } from 'next/router';
import React, {  useEffect, useState } from 'react';
import styled from 'styled-components';
const Container = styled.div`
  width: 100%;
  height: 100%;
  display:flex;
  margin: 15px;
  flex-direction: column;
  align-items: center;
  color: black;
  background-color: white;
`; 
const Title = styled.h1`

`;
const Input = styled.input`
  margin: 5px 0px;
  width: 100%;
  padding: 5px;
  border-radius: 5px;
  border: 1px solid black;
`; 
const ReturnButton = styled.div`
  padding: 5px;
  margin: 5px;
  background-color: #0274d9;
  color: white;
  border-radius: 5px;
  cursor: pointer;
`;

function NotFounded() {
  const router = useRouter();
  return (
    <Container>
     {"Oups cette page n'existe pas ! :("}
     <ReturnButton> Retourner à l'accueil </ReturnButton>
    </Container>
  );
}
//impossible d'utiliser serverside props


export default NotFounded;
