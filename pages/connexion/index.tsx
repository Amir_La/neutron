import React, {  useEffect, useState } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import { useRouter } from 'next/router'
import Login from './login';
import SignUp from './signup';
import { useCookies } from "react-cookie"
import { parseCookies } from '../../helpers/cookies';


const Container = styled.div`
  width: 100%;
  height: calc(100vh - 60px);
  display:flex;
  flex-direction: column;
  align-items:item;
  color: black;
  background-color: white;
`; 
const Title = styled.h1`
  width: 100%;
  text-align:center;
`;
const ConnexionContainer = styled.div`
  width: 70%;
  margin: 0 auto;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
const ChangePage = styled.div`
  background-color: #1446A0;
  color: white;
  padding: 5px;
  cursor:pointer;
  border-radius: 5px;
`;

const SignUpDone = styled.div`
  padding: 5px;
  color: green;
`;

type userConnection = {
  mail: string;
  password: string;
};

type userSignup = {
  mail: string;
  password: string;
  username: string;
};

function Connexion(props: any) {
  const [isSignUpPage,setIsSignUpPage] = useState<boolean>(false);
  const [error,setError] = useState<boolean>(false);
  const [signUpDone,setSignUpDone] = useState<boolean>(false);
  const router = useRouter();
  const [cookie, setCookie] = useCookies(["user"]);

  const connectUser = (user: userConnection) => {

    axios.post(`${process.env.NEXT_PUBLIC_URL}/users/signin`,{...user})
    .then(response => {
      console.log(response);
      if(response.data && response.data.accessToken)
      {
        try {
          setCookie("user", JSON.stringify(response.data), {
            path: "/",
            maxAge: 3600, //1 hour
            sameSite: true,
          });
          router.push("/");
        }
        catch(e){
          console.log("cookie");
          console.error(e);
          
        }
      } 
    })
    .catch(e => {
      console.log(e);
      setError(true);
    
    });
  }
  useEffect(() => {
    if(props.data && props.data.user)
    {
        router.push("/");
    }
  },[cookie])

  const signUpUser = (user: userSignup) => {
    axios.post(`${process.env.NEXT_PUBLIC_URL}/users/createUser`,{...user})
    .then(response => {
      console.log(response);
      if(response.status === 201)
      {
        setIsSignUpPage(false);
        setSignUpDone(true);
      } 
    })
    .catch(e => {
      console.log(e);
      setError(true);
    });
  }

  return (
    <Container>
      <Title> {isSignUpPage ? "Inscription"  : "Connexion"} </Title>
      
      <ConnexionContainer>
        { signUpDone && <SignUpDone> Votre inscription a été complété avec succès veuillez vous connecter désormais </SignUpDone> }
        {
          isSignUpPage ?
          <SignUp error={error} signUpUser={(user: userSignup) => signUpUser(user)} />
          :
          <Login error={error} connectUser={(user: userConnection) => connectUser(user)} />
        }
        <ChangePage onClick={() => setIsSignUpPage(!isSignUpPage)} > {  isSignUpPage ? "Se Connecter" : "S'inscrire" } </ChangePage>
      </ConnexionContainer>

    </Container>
  );
}

Connexion.getInitialProps =  async ({ req, res }: any)=> {
  const data = parseCookies(req)
  if (res) {
      if (Object.keys(data).length > 0) {
        res.writeHead(301, { Location: "/" })
        res.end()
      }
   }

    return {
      data: data && data,
    }
}
export default Connexion;
