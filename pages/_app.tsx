import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Layout from '../components/Layout'
import { CookiesProvider } from "react-cookie"

function MyApp({ Component, pageProps }: AppProps) {
  return (
  <CookiesProvider>
    <Layout>
      <Component {...pageProps} />
    </Layout>
  </CookiesProvider>
  )
}
export default MyApp
