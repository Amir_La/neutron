import React, {  useEffect, useState } from 'react';
import styled from 'styled-components';
import BookItem from '../components/BookItem';
import Navbar from '../components/navbar';

const Container = styled.div`
  width: 100%;
  height: calc(100vh - 60px);
  display:flex;
  flex-direction: column;
  align-items:item;
  color: #272727;
  background-color: #fff;
`; 
const Title = styled.h1`
  width: 100%;
  text-align:center;
`;
const BooksContainer = styled.div`
  width: 70%;
  margin: 0 auto;
  height: 100%;
`;
const LabelBooks = styled.div`

`;

const Books = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  margin-top: 55px;
`;


type Book = {
  name: string;
  author: string;
  chapters: any[];
  types: any[];
  description: string;
  id: number;
}

type IBooksProps = {
  books: Book[];
}

export default function Home(props: IBooksProps) {
  const { books } = props;
  return (
    <Container>
      <Title> Bienvenue dans Neutron votre bibliothèque virtuelle</Title>
      
      <BooksContainer>
        <LabelBooks> Voici les livres du moment : </LabelBooks>
        <Books>
          {
          books.length > 0 ? 
          books.map(
            (book: Book) => <BookItem
            name={book.name}
            path={"books"}
            author={book.author}
            chapters={book.chapters.length}
            types={book.types as any[]}
            id={book.id}
            content={"Consulter"}
            description={book.description}
            />
          ) : " Pas de livres à présenter :(."
          }
        </Books>
      </BooksContainer>
    </Container>
  )
}

export async function getStaticProps(context: any) {

  const res = await fetch(`${process.env.NEXT_PUBLIC_URL}/books`)
  const books : Book[] = await res.json()
  if (!books) {
      return {
          redirect: {
              destination: '/',
              permanent: false,
          },
      }
  }
  return {
      props: { books }, // will be passed to the page component as props
      revalidate : 15
  }
}


