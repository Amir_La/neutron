import React, {  useState } from 'react';
import { useCookies } from 'react-cookie';
import styled from 'styled-components';
import { parseCookies } from '../../helpers/cookies';
import BookItem from '../../components/BookItem';
import axios from 'axios';

const Container = styled.div`
  width: 100%;
  height: 100%;
  display:flex;
  flex-direction: column;
  align-items: center;
  color:black;
  background-color: white;
`;
const Name = styled.div`
`;
const MyBookContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 15px 0px;
  justify-content: center;
  align-items: center;
`;
const MyFavContainer = styled.div`
`;
const AddBookContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 15px;
  border-radius: 5px;
  background-color: #E5E5E5;
`;
const AddBookButton = styled.button`
  padding: 5px;
  border: 1px solid #023e8a;
  background-color: white;
  border-radius: 5px;
  cursor: pointer;
  max-width: 250px
`;
const NameInput = styled.input`
`;
const DescriptionInput = styled.textarea`

`;

const TypeInput = styled.select`

`;
const ConfirmButton = styled.button`
  padding: 5px;
  background-color: #7DC95E;
  color: white;
  border-radius: 5px;
  cursor: pointer;
  margin: 5px 0px;
  max-width: 250px;
  border: 0;
`;
const Label = styled.label`
`;
const Title = styled.div`
  font-weight: 900;
  font-size: 20px;
`;
const Books = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
`;
const BookContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 5px;
`;
const SuppressButton = styled.div`

  border: 3px solid red;
  color: red;
  border-radius: 10px;
  width: 100%;
  padding: 5px;
  text-align: center;
  cursor: pointer;
`;
const EditButton = styled.div`
  border: 3px solid blue;
  color: blue;
  border-radius: 10px;
  width: 100%;
  padding: 5px;
  margin: 10px 0px;
  text-align: center;
  cursor: pointer;
`;
type Profile = {
  id: number;
  username: string,
  mail: string,
  books: any,
  books_fav: any
};
function Profil(props: any) {
  const { profile,types } = props;
  let userProfile: Profile = profile;
  const [cookies, setCookie, removeCookie] = useCookies(["user"]);
  const [openAddBook, setOpenAddBook] = useState(false);
  const [myBooks,setMyBooks] = useState(userProfile.books);
  //AddBookForm
  const [author,setAuthor] = useState("");
  const [selectedBookId,setSelectedBookId] = useState(false);
  const [name,setName] = useState("");
  const [description,setDescription] = useState("");
  const [type,setType] = useState(1);
  const [openModifyBook,setOpenModifyBook] = useState(false);

  const handleAddBook = () => {
   const option = {
      headers: {'Authorization': `Bearer ${cookies.user.accessToken}`}
    }
    axios.post(`${process.env.NEXT_PUBLIC_URL}/books/addBook`,{name,description,type,author: userProfile.username},option)
    .then((response: any) => {
        const { data } = response;
        let newBooks = [...data];
        console.log("before setState : ",newBooks);
        setMyBooks([...newBooks]);
        console.log(myBooks);
        setName("");
        setDescription("");
        setType(1);
        setOpenAddBook(false);
    })
    .catch(e => console.error(e));
  }
  const handleEditBook = () => {
   const option = {
      headers: {'Authorization': `Bearer ${cookies.user.accessToken}`}
    }
    axios.patch(`${process.env.NEXT_PUBLIC_URL}/books/updateBook`,{id: selectedBookId,name,description,author},option)
    .then((response: any) => {
        const { data } = response;
        let newBooks = [...data];
        console.log("before setState : ",newBooks);
        setMyBooks([...newBooks]);
        console.log('after : ',myBooks);
        setName("");
        setDescription("");
        setAuthor("");
        setOpenModifyBook(false);
    })
    .catch(e => console.error(e));
  }
  const handleSuppressBook = (id: number) => {
    const option = {
      headers: {'Authorization': `Bearer ${cookies.user.accessToken}`}
    }
    axios.delete(`${process.env.NEXT_PUBLIC_URL}/books/${id}`,option)
    .then((response: any) => {
        setMyBooks([...myBooks.filter((book: any) => book.id != id)]);
    })
    .catch(e => console.error(e));
  }
  const handleOpenEditBook = (book: any) => {
    setDescription(book.description);
    setName(book.name);
    setAuthor(book.author);
    setSelectedBookId(book.id);
    setOpenModifyBook(true);
  }
  return (
    <Container>
      <Name>
        Profil de {userProfile.username}
      </Name>
      <MyBookContainer>
        <Title> Mes Livres : </Title>
        <Books>
           {myBooks.length > 0 ? myBooks.sort((a:any,b:any) => a.id - b.id).map((book: any) => <BookContainer>
           <BookItem
              name={book.name}
              path={"profilebook"}
              author={book.author}
              chapters={book.chapters ? book.chapters.length : 0}
              types={book.types as any[]}
              id={book.id}
              content={"Chapitres"}
              description={book.description}
              /><EditButton onClick={() => handleOpenEditBook(book)}> Modifier  </EditButton> <SuppressButton onClick={() => handleSuppressBook(book.id)}> Supprimer  </SuppressButton> </BookContainer> ) : "Vous ne disposez encore d'aucun livre" }
        </Books>
        <AddBookButton onClick={() => setOpenAddBook(!openAddBook)}> {!openAddBook ? "Ajouter un livre" : "Retour" }  </AddBookButton> 
        {
          (openAddBook || openModifyBook )  && 
          <AddBookContainer>
            <Label> Titre du Livre </Label>
            <NameInput value={name} onChange={(e: any) => setName(e.target.value)} />
            <Label> Genre du Livre </Label>
            {
              !openModifyBook ?
              <TypeInput onChange={(e: any) => setType(e.target.value)}>
                {
                  types.map((type: any) => <option value={type.id} label={type.name} />)
                }  
              </TypeInput> : 
              <>
               <Label> Auteur du Livre </Label>
               <NameInput value={author} onChange={(e: any) => setAuthor(e.target.value)} />
              </>
            }
            <Label> Description du Livre </Label>
            <DescriptionInput value={description} onChange={(e: any) => setDescription(e.target.value)} />
            <ConfirmButton onClick={() => openAddBook ? handleAddBook() : handleEditBook()}> Confirmer </ConfirmButton>
          </AddBookContainer>
        }
      </MyBookContainer>
      <MyFavContainer>
        <Title> Mes Favoris : </Title> {userProfile.books_fav.length > 0 ? userProfile.books_fav.map((fav: any) => fav) : "Vous ne disposez encore d'aucun favori" }
      </MyFavContainer>
    </Container>
  );
}
export async function getServerSideProps({ req, res }: any) { 
  const data = parseCookies(req);
  if(data && data.user) {
    const dataParsed = JSON.parse(data.user);
    const options = {
      method: "GET",
      headers: {
            'Authorization': `Bearer ${dataParsed.accessToken}`
      }
    }
    const responseProfile = await fetch(`${process.env.NEXT_PUBLIC_URL}/users/profile/${dataParsed.id}`,options)
    const profileInfo: Profile = await responseProfile.json();
    const responseTypes = await fetch(`${process.env.NEXT_PUBLIC_URL}/types`);
    const types = await responseTypes.json();
    return {
      props: { 
        profile: profileInfo,
        types
      }
    }
  }
  else{
    return {
      redirect: {
          destination: '/connexion',
          permanent: false,
      },
    }
  }
}



export default Profil;
