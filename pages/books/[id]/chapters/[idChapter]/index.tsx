import { useRouter } from 'next/router';
import React, {  useEffect, useState } from 'react';
import styled from 'styled-components';
const Container = styled.div`
  width: 100%;
  height: 100%;
  display:flex;
  padding: 5px;
  flex-direction: column;
  align-items: center;
  color: black;
  background-color: white;
`; 
const Title = styled.h1`

`;
const ContentContainer = styled.div`
  width: 100%;
  height: 100%;
  @media screen and (min-width: 767px){
    width: 50%;
    margin: 0 auto;
  }
`;
const HeadContainer = styled.div`

`;
const ReturnButton = styled.button`
  padding: 5px 15px;
  background-color: #14213D;
  border-radius: 5px;
  color: #fff;
  border: 0;
  cursor: pointer;
`;

function Chapter(props: any) {
  const router = useRouter();
  const { id,idChapter } = router.query;
  const { title,index,content } = props.data;
  useEffect(() => {
    console.log(props)
    
  }, [])
  return (
    <Container>
      <HeadContainer>
        <ReturnButton onClick={() => router.back()}> Retour </ReturnButton>
        <Title>{idChapter} - {title}</Title>
      </HeadContainer>
      <ContentContainer dangerouslySetInnerHTML={{__html:content}} />
    </Container>
  );
}
//impossible d'utiliser serverside props

export async function getServerSideProps({ query }: any) {
  const { id,idChapter } = query;
  const res = await fetch(`${process.env.NEXT_PUBLIC_URL}/chapters/${id}/${idChapter - 1}`,{method: "GET"})
  const data = await res.json()

  if (!data) {
    return {
      notFound: true,
    }
  }

  return {
    props: { data }
  }
}

export default Chapter;
