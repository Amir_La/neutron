import { useRouter } from 'next/router';
import React, {  useEffect, useState } from 'react';
import styled from 'styled-components';
import ChapterItem from '../../../components/ChapterItem';
const Container = styled.div`
  width: 100%;
  height: 100%;
  display:flex;
  margin: 15px;
  flex-direction: column;
  align-items: center;
  color: black;
  background-color: white;
`; 
const Title = styled.h1`

`;
const Description = styled.p`

`;
const Author = styled.p`

`;
const Chapters = styled.div`

`;
const Info = styled.p`

`;

function Book(props: any) {
  const router = useRouter();
  const { author,chapters,id,name,description,types,user_author } = props.data;
  //const [cookies, setCookie, removeCookie] = useCookies(["user"]);
  console.log(props.data);

  return (
    <Container>
      <Title> {name} </Title>
      <Author> de {author}  </Author>
      <Description>
        {description}
      </Description>
      <Chapters>
        {chapters.length > 0 ? chapters.map((chap: any) => <ChapterItem index={chap.index + 1}  title={chap.title} idBook={id} />) : <Info> {"Le livre ne dispose d'aucun chapitre :("} </Info>}
      </Chapters>
    </Container>
  );
}
//impossible d'utiliser serverside props


export async function getServerSideProps({ query }: any) {
  const { id } = query;
  
  const res = await fetch(`${process.env.NEXT_PUBLIC_URL}/books/${parseInt(id)}`,{method: "GET"});
  const data = await res.json();

  return {
    props: { data }
  }
  
}
export default Book;
