import { useRouter } from 'next/router';
import React, {  useEffect, useState } from 'react';
import styled from 'styled-components';
import EditChapter from '../../../components/EditChapter';
import { parseCookies } from '../../../helpers/cookies';
import { EditorState } from 'draft-js';
import { useCookies } from 'react-cookie';
import axios from 'axios';
import ChapterItem from '../../../components/ChapterItem';
const Container = styled.div`
  height: 100%;
  display:flex;
  margin: 15px;
  flex-direction: column;
  align-items: center;
  color: black;
  background-color: white;
`;
const Title = styled.h1`
  font-size: 20px;
  font-weight: 900;
`;
const Description = styled.p`

`;
const Author = styled.p`

`;
const Chapters = styled.div`

`;
const Info = styled.p`

`;
const AddChapterContainer = styled.div`

`;
const BookContainer = styled.div`

`; 
const AddChapterButton = styled.button`
  border: 1px solid #0274d9;
  color: #0274d9;
  border-radius: 5px;
  background-color: white;
  width: 100%;
  cursor: pointer;
  padding: 5px;
  text-align: center;
`;
const ConfirmButton = styled.div`
  border: 3px solid #0274d9;
  font-weight: 900;
  width: 250px;
  color: #0274d9;
  border-radius: 5px;
  cursor: pointer;
  padding: 5px;
  text-align: center;
  margin: 5px 10px;
`;
const CancelButton = styled.div`
  border: 3px solid #f37342;
  font-weight: 900;
  width: 250px;
  color: #f37342;
  border-radius: 5px;
  cursor: pointer;
  padding: 5px;
  text-align: center;
  margin: 5px 10px;
`;
const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;
const Input = styled.input`
  width: 100%;
  padding: 10px;
  margin: 5px 0px;
`;
const Label = styled.div`
  padding: 5px;
  font-weight: 900;
`;
const InfoContainer = styled.div`
  width: 100%;
  padding: 5px;
`;


function UserBook(props: any) {
  const router = useRouter();
  //const [cookies, setCookie, removeCookie] = useCookies(["user"]);
  const { author,chapters,id,name,description,types,user_author } = props.data;
  const [stateChapters, setStateChapters] = useState([...chapters])
  const [openAddChapter, setOpenAddChapter] = useState(false);
  const [title, setTitle] = useState("");
  const [chapterContent,setChapterContent] = useState(EditorState.createEmpty());
  const [cookies, setCookie, removeCookie] = useCookies(["user"]);
   
  const saveChapterContent = () => {
      let newChapter = {book: id,title,index: stateChapters.length,content: chapterContent};
      const option = {
        headers: {'Authorization': `Bearer ${cookies.user.accessToken}`}
      }
      axios.post(`${process.env.NEXT_PUBLIC_URL}/chapters/addChapter`,{book: id,title,index: stateChapters.length,content: chapterContent},option)
      .then((response: any) => {
        console.log(response);
        setStateChapters([...stateChapters,newChapter]);
        setOpenAddChapter(false);
      })
      .catch(e => console.error(e))
  }
  const openChapter = () => {
    setOpenAddChapter(true);
  }



  return (
    <Container>
      {
        !openAddChapter ? 
        <BookContainer>
          <Title> {name} </Title>
          <Author> de {author}  </Author>
          <Description>
            {description}
          </Description>
          {
            !openAddChapter && <AddChapterButton onClick={() => openChapter()} >  Ajouter un Chapitre </AddChapterButton>
          }
          <Chapters>
            {stateChapters.length > 0 ? stateChapters.map((chap: any) => <ChapterItem index={chap.index + 1}  title={chap.title} idBook={id} /> ) : <Info> {"Le livre ne dispose d'aucun chapitre :("} </Info>}
          </Chapters>
        </BookContainer>
        :
        <AddChapterContainer>
          <Title> Edition de Chapitre </Title>
          <InfoContainer>
            <Label> Titre du chapitre </Label>
            <Input value={title} onChange={(data) => setTitle(data.target.value)} />
          </InfoContainer>
          <EditChapter value={chapterContent} onChange={(e: any) => setChapterContent(e)} />
          <ButtonContainer>
            <CancelButton onClick={() => {setOpenAddChapter(false); }} > Annuler </CancelButton>
            <ConfirmButton onClick={() => saveChapterContent()}> Enregistrer </ConfirmButton>
          </ButtonContainer>
        </AddChapterContainer>
      }
    </Container>
  );
}
//impossible d'utiliser serverside props

export async function getServerSideProps({ res,req,query }: any) {
  const { id } = query;
  const data = parseCookies(req);
  if(data && data.user) {
    const dataParsed = JSON.parse(data.user);
    const options = {
      method: "GET",
      headers: {
            'Authorization': `Bearer ${dataParsed.accessToken}`
      }
      
    }
    const auth = await fetch(`${process.env.NEXT_PUBLIC_URL}/books/ismybook/${parseInt(id)}`,options);
    const authResponse = await auth.status;
    console.log("your respones is : ",authResponse);
    if(authResponse == 200)
    {
      const res = await fetch(`${process.env.NEXT_PUBLIC_URL}/books/${parseInt(id)}`,{method: "GET"});
      const data = await res.json();
      return {
        props: { data }
      }
    }
    else if(authResponse == 401){
      return {
        redirect: {
            destination: '/notAuthorized',
            permanent: false,
        },
      }
    }
    else {
      return {
        redirect: {
            destination: '/notFound',
            permanent: false,
        },
      }
    }
  }
  else{
    return {
      redirect: {
          destination: '/connexion',
          permanent: false,
      },
    }
  }
  
}

export default UserBook;
